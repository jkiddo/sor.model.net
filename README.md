sor.model.net
================

This is the Sor model made easy - probably the only project of its kind and purpose: To make it easier for vendors in the danish healthcare sector to use the Sor data model maintained by NSI.

This project fetches the current versions of the Sor schemas, generates classes from the xsd's and parses them into a .NET library - thereby making it way easier to parse the xml on http://filer.sst.dk/sor/data/sor/sorxml/v_2_0_0/Sor.zip. Just call the script.bat from a **Visual Studio** ***Developer Command Prompt*** and you should have a library in just about no time.

Happy codin'.